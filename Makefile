CC = gcc
CFLAGS = -Wall -g 
LIB = -lcrypto

all: server client

client: client.o
	$(CC) -o client $^ $(LIB)
	
server: server.o
	$(CC) -o server $^ $(LIB)

.c.o:
	$(CC) $(CFLAGS) -c -o $@ $<

clear:
	rm -rf *.o server client
	
	