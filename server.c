#include "times.h"

char k[8] = {'c','i','a','o','l','u','c','a'};


/* stampa un byte in esadecimale */
void printbyte(char b) {
	char c;
	c = b;
	c = c >> 4;
	c = c & 15; printf("%X", c); c = b;
	c = c & 15; printf("%X:", c);
}



int answerToClient(int sk)
{
	int ret, len = 9;
	char request[10], msg[26];
	time_t now = time(NULL);
	
	char *ciphertext;
	int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	int i; /* index */
	int pt_len; /* plain text size */
	int ct_len; /* encrypted text size */
	int ct_ptr; /* first available entry in the buffer */
	int msg_len; /* message length */
	
	//azzeramento di request
	memset((void*) request, ' ',10); 
	
	//creazione stringa con data
	ctime_r(&now,msg);
	
	//ricezione della richiesta dal client
	ret = recv(sk, (void *) request, len,0);
	
	//Stampa della richiesta
	printf("\nRichiesta del client: %s",request);
	
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
	
	
	
	/* Context allocation */
	EVP_CIPHER_CTX *ctx = (EVP_CIPHER_CTX *)malloc(sizeof(EVP_CIPHER_CTX));
	
	/* Context initialization */
	EVP_CIPHER_CTX_init(ctx);
	
	/* Context setup for encryption */
	EVP_EncryptInit(ctx, EVP_des_ecb(), NULL, NULL);

	/* Output of the original message */
	printf("\nOriginal message:\n%s\n\n", msg);
	
	/* Output of the encryption key size */
	printf("Key size %d\n", EVP_CIPHER_key_length(EVP_des_ecb()));
	
	/* Output of the block size */
	printf("Block size %d\n\n", EVP_CIPHER_CTX_block_size(ctx));
	

	
	/* Encryption key set up */ 
	EVP_EncryptInit(ctx, NULL, k, NULL);
	
	/* Buffer allocation for the encrypted text */
	msg_len = strlen(msg)+1;
	ct_len = msg_len + EVP_CIPHER_CTX_block_size(ctx);
	printf("Message size %d\n", msg_len);
	printf("Ciphertext size %d\n", ct_len); 
	ciphertext = (char *)malloc(ct_len);



	/* Encryption */
	nc = 0;
	nctot = 0;
	ct_ptr = 0;
	EVP_EncryptUpdate(ctx, ciphertext, &nc, msg, msg_len);
	ct_ptr += nc;
	nctot += nc;
	EVP_EncryptFinal(ctx, &ciphertext[ct_ptr], &nc); 
	nctot += nc;
	printf("\nCiphertext:\n"); 
	
	for (i = 0; i < ct_len; i++)
		printbyte(ciphertext[i]); 
	printf("\n\n");
	
	
	
	
	
	//invio risposta
	ret = send(sk, (void*) ciphertext, ct_len,0);
	
	//controllo invio richiesta
	if(ret == -1 || ret < strlen(msg))
	{
		perror("\nErrore di invio dati");
		return -1;
	}
	
	//Stampa informazioni e chiusura connessione	
	printf("\nDati inviati al client con successo\n");
	close(sk);
	
	free(ctx);
	free(ciphertext);
	
	return 0;
}

int main(void)
{
	//allocazione delle strutture dati necessarie
	struct sockaddr_in my_addr, cl_addr;
	int ret, len, sk, cn_sk;
	
	//Verbose
	printf("Avvio del server\n");
	
	//creazione del socket di ascolto
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sk == -1)
	{
		perror("Errore nella creazione del socket");
		return 0;
	}
	else
		printf("Socket di ascolto creato correttamente\n");
	
	//inizializzazione delle strutture dati
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(1234);
	
	//bind del socket
	ret = bind(sk, (SA *) &my_addr, sizeof(my_addr));
	
	if(ret == -1)
	{
		perror("Errore nell'esecuzione della bind del socket");
		return 0;
	}
	else
		printf("Bind del socket eseguita correttamente\n");
		
	//messa in ascolto del server sul socket
	ret = listen(sk, 10);
	
	if(ret == -1)
	{
		perror("Errore nella messa in ascolto del server");
		return 0;
	}
	else
		printf("Server messo in ascolto sul socket correttamente\n");
	
	//dimensioni della struttura dove viene salvato l'ind del client
	len = sizeof(cl_addr);
	
	//ciclo in cui il server accetta connessioni in ingresso e le gestisce
	for(;;)
	{
		//accettazione delle connessioni ingresso
		cn_sk = accept(sk, (SA *) &cl_addr, (socklen_t *) &len);
		
		if(cn_sk == -1)
		{
			perror("Errore nell'accettazione di una richiesta");
			return 0;
		}
		else
			printf("Richiesta accettata correttamente\n");
		
		//gestione delle richieste
		answerToClient(cn_sk);
	}
	
	//Chiusura del socket di ascolto
	//Questo codice non verrà mai eseguito dato che si trova
	//dopo un ciclo infinito
	
	printf("\nChiusura del server\n");
	close(sk);
	return 0;	
}
