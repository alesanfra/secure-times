#include "times.h"

char k[8] = {'c','i','a','o','l','u','c','a'};


/* stampa un byte in esadecimale */
void printbyte(char b) {
	char c;
	c = b;
	c = c >> 4;
	c = c & 15; printf("%X", c); c = b;
	c = c & 15; printf("%X:", c);
}

/* chiede al server l'ora, ritorna 0 se tutto è andato a buon fine */
int askToServer()
{
	//dichiarazioni delle variabili
	struct sockaddr_in srv_addr;
	int ret, sk;
	char *request = "GET_TIME";
	char msg[64];
	char ip[20];
	int porta;
	
	char *plaintext, *ciphertext;
	int nc; /* amount of bytes [de]crypted at each step */
	int nctot; /* total amount of encrypted bytes */
	int i; /* index */
	int pt_len; /* plain text size */
	int ct_len = 34; /* encrypted text size */
	int ct_ptr; /* first available entry in the buffer */
	int msg_len; /* message length */
	
	/* Context allocation */
	EVP_CIPHER_CTX* ctx = (EVP_CIPHER_CTX *)malloc(sizeof(EVP_CIPHER_CTX));

	
	//inserimento indirizzo IP e porta
	printf("Inserisci indirizzo IP e porta: ");
	scanf("%s %i",ip,&porta);
	
	//creazione del socket TCP
	sk = socket(AF_INET, SOCK_STREAM, 0);
	
	//azzeramento della struttura
	memset(&srv_addr, 0, sizeof(srv_addr));
	
	//inserimento nella struttura dell'Ip e porta
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(porta);
	ret = inet_pton(AF_INET,ip, &srv_addr.sin_addr);
	
	//connessione al server
	ret = connect(sk, (SA *) &srv_addr, sizeof(srv_addr));
	
	//controllo connessione
	if(ret == -1)
	{
		perror("\nErrore di connessione");
		return -1;
	}
	
	//invio richiesta
	ret = send(sk, (void*) request, strlen(request)+1,0);
	
	//controllo invio richiesta
	if(ret == -1 || ret < strlen(request))
	{
		perror("\nErrore di invio dati");
		return -1;
	}
	
	
		
	//ricezione messaggio
	ret = recv(sk, (void *) msg, ct_len, MSG_WAITALL);
	
	if(ret == -1)
	{
		perror("\nErrore ricezione pacchetto");
		return -1;
	}
	
	/* compatibilità con il codice esistente */
	ciphertext = msg;	
	
	/* Decryption context initialization */ 
	EVP_CIPHER_CTX_init(ctx);
	EVP_DecryptInit(ctx, EVP_des_ecb(), k, NULL);

	pt_len = ct_len + EVP_CIPHER_CTX_block_size(ctx); 	
	plaintext = (char *)malloc(pt_len);
	
	
	/* Decryption */
	nc = 0; nctot = 0;
	ct_ptr = 0;
	EVP_DecryptUpdate(ctx, &plaintext[ct_ptr], &nc, ciphertext, ct_len); 
	ct_ptr += nc;
	nctot += nc;
	EVP_DecryptFinal(ctx, &plaintext[ct_ptr], &nc); 
	nctot += nc;

	for (i = 0; i < pt_len - 1; i++) 
		printf("%c:", plaintext[i]);
		
	printf("%c\n", plaintext[pt_len-1]);
		
	printf("\n%s\n\n", plaintext);
	EVP_CIPHER_CTX_cleanup(ctx);
	

	close(sk);
	free(ctx);
	free(plaintext);
	
	return 0;
}



int main(void)
{
	int result;
	char stop = 'y';
	
	do{
		//richiesta dell'ora al server
		printf("\n\nRichiesta dell'ora al server ...\n");
		result = askToServer();
		
		//prompt
		printf("Richiedere di nuovo l'ora al server? (Y,n): ");
		scanf("\n%c",&stop);
		
	} while(stop != 'n');
	
	printf("\nChiusura client in corso...\n");
	return 0;
}
